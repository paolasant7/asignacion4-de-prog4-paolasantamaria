redis 127.0.0.1:6379> ping
PONG
127.0.0.1:6379> SET Bulto Persona
OK
127.0.0.1:6379> SET Chaniao Vestido
OK
127.0.0.1:6379> SET Parranda Fiesta
OK
127.0.0.1:6379> SET Lagarto Picaro
OK
127.0.0.1:6379> mget Bulto
1) "Persona"
127.0.0.1:6379> mget lagarton
1) (nil)
127.0.0.1:6379> mget Parranda
1) "Fiesta"
127.0.0.1:6379> mget Chantin
1) "Casa"
127.0.0.1:6379> mget Bulto Chantin Parranda
1) "Persona"
2) "Casa"
3) "Fiesta"
127.0.0.1:6379> HSET diccionariops taller3 "Pasar al taller4"
(integer) 1
127.0.0.1:6379> HSET diccionariops tarea4 "Poniendo en practica el uso de Redis"
(integer) 1
127.0.0.1:6379> HSET diccionariops Uni "En la Universidad Latina"
(integer) 1
127.0.0.1:6379> HGETALL diccionariops
1) "taller3"
2) "Pasar al taller4"
3) "tarea4"
4) "Poniendo en practica el uso de Redis"
5) "Uni"
6) "En la Universidad Latina"
127.0.0.1:6379> BGSAVE
Background saving started
127.0.0.1:6379> hum.rdb

